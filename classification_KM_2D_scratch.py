import numpy
import random
import copy
import matplotlib.pyplot as plt

def distance(points):
    sum = 0
    n = len(points)
    p0 = points[0]
    m = len(p0)
    for i in range(1,n):
        p = points[i]
        for j in range(m):
            sum += abs(p0[j] - p[j])
    return sum

def center(points):
    n = len(points)
    m = len(points[0])
    p0 = [0 for i in range(m)]
    for i in range(n):
        p = points[i]
        for j in range(m):
            p0[j] += p[j] / n
    return p0

def classification(centers,points):
    Z = []
    for i in range(len(points)):
        p = points[i]
        p0 = centers[0]
        min_distance = distance([p0,p])
        min_index = 0
        for j in range(1,len(centers)):
            p0 = centers[j]
            calc_distance = distance([p0,p])
            if calc_distance < min_distance:
                min_distance = calc_distance
                min_index = j
        Z.append(min_index)
    return Z

def plot(L,color):
    plt.plot(numpy.matrix.transpose(numpy.array(L))[0],numpy.matrix.transpose(numpy.array(L))[1],'ro',c=color)

def affiche(Y):
    for y in Y:
        if len(y)==0:
            continue
        color = [random.random(), random.random(), random.random()]
        plot(y, color)
    plt.show()

def range_classes(X, Z):
    Y = [[] for i in range(n)]  # Classes
    for i in range(len(Z)):
        x = X[i]
        z = Z[i]
        Y[z].append(x)
    return Y

def affect_classes(X):
    W = [random.choice(X) for i in range(n)]  # Start centers
    Z = []  # Save of Y
    while True:
        Z_save = copy.deepcopy(Z)
        Z = classification(W, X)
        if Z == Z_save:
            break
        for i in range(n):
            L = []
            for j in range(len(Z)):
                z = Z[j]
                if z != i:
                    continue
                L.append(X[j])
            if len(L) > 0:
                W[i] = center(L)
    return Z
def main(X):
    Z = affect_classes(X)
    Y = range_classes(X, Z)
    affiche(Y)

if __name__ == "__main__":
    random.seed(0)  # Permet d'avoir du pseudo aléatoire
    n = 10  # Nombre de classes
    p = 1000  # Nombre de points
    X = [[random.random(), random.random()] for i in range(p)]  # Input data
    main(X)
