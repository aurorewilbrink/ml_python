import numpy as np
import sklearn.neural_network as nn

# Function to approximate
def f(x):
    return x**2 + np.sin(5*x)

# Synthetic dataset generation
inf_bound = 0
sup_bound = np.pi
N_train = 100
N_test = 20
X = np.array([[np.random.uniform(inf_bound, sup_bound)] for i in range(N_train)])
Y = np.array([f(x) for x in X])

# Neural Network Learning process
model = nn.MLPRegressor(hidden_layer_sizes = (10, 10, 10, 10), activation = 'tanh', solver = 'lbfgs', max_iter = 100000)
model.fit(X, Y)
# Neural network data test set
X_test = np.array([[np.random.uniform(inf_bound, sup_bound)] for i in range(N_test)])
Y_test = np.array([f(x) for x in X_test])
# Neural network prediction
Y_pred = model.predict(X_test) 
Y_pred = np.reshape(Y_pred,(N_test,1)) 
errors = np.abs(Y_test - Y_pred)

print('Mean test error :', np.mean(errors[:, 0]))
print('Max test error :', np.max(errors[:, 0]))