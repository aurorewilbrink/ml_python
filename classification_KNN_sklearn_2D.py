import numpy
import random
import matplotlib.pyplot as plt
from sklearn.datasets import load_iris
from sklearn.model_selection import train_test_split
from sklearn.neighbors import KNeighborsClassifier  # https://scikit-learn.org/stable/modules/generated/sklearn.neighbors.KNeighborsClassifier.html#sklearn.neighbors.KNeighborsClassifier.get_params

#data form
p = 50
R = 2
t = numpy.linspace(0, 2*numpy.pi, p)
X=[]
Y=[]
for n in range(p):
    X.append([
        (1+R*random.random()*numpy.cos(t[n]))/(1+R),
        (1+R*random.random()*numpy.sin(t[n]))/(1+R)
    ])
    Y.append(0)
for n in range(p):
    X.append([
        (-1+R*random.random()*numpy.cos(t[n]))/(1+R),
        (-1+R*random.random()*numpy.sin(t[n]))/(1+R)
    ])
    Y.append(1)
Z=["bla","blo"]

#train
knn=KNeighborsClassifier(n_neighbors=1)  # create the neural network
knn.fit(X,Y)  # train the neural network

#test new
m=50
X_test=[]
for i in range(m):
    for j in range(m):
        X_test.append([-1+2*i/m,-1+2*j/m])
X_test=numpy.array(X_test)
Y_test = knn.predict(X_test)  # calcul the outputs
x1=[]
x2=[]
for i in range(len(Y_test)):
    x = X_test[i]
    y = Y_test[i]
    if y:
        x1.append(x)
    else:
        x2.append(x)
print(Y_test)
# print(prediction)

#plot
plt.plot(numpy.matrix.transpose(numpy.array(X))[0][:len(X)//2],numpy.matrix.transpose(numpy.array(X))[1][:len(X)//2],'ro',c=[1,0,0])
plt.plot(numpy.matrix.transpose(numpy.array(X))[0][len(X)//2:],numpy.matrix.transpose(numpy.array(X))[1][len(X)//2:],'ro',c=[0,1,0])
plt.plot(numpy.matrix.transpose(numpy.array(x1))[0],numpy.matrix.transpose(numpy.array(x1))[1],'+',c=[0,1,0])
plt.plot(numpy.matrix.transpose(numpy.array(x2))[0],numpy.matrix.transpose(numpy.array(x2))[1],'+',c=[1,0,0])

plt.show()

