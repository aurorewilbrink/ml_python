import numpy as np
import sklearn.neural_network as nn
import matplotlib

np.random.seed(0)

# Function to approximate
def f(O1,O2):
    x = np.sin(O1) + np.sin(O1+O2)
    y = np.cos(O1) + np.cos(O1+O2)
    return [x, y]

# Synthetic dataset generation
inf_bound = 0
sup_bound = np.pi

N_train = 100
N_test = 10
X = np.array([[np.random.uniform(inf_bound, sup_bound),np.random.uniform(inf_bound, sup_bound)] for i in range(N_train)])
Y = np.array([f(O1,O2) for O1,O2 in X])

# Neural Network Learning process
model = nn.MLPRegressor(hidden_layer_sizes = (10, 10, 10, 10), activation = 'tanh', solver = 'lbfgs', max_iter = 100000)
model.fit(Y, X)
# Neural network data test set
X_test = np.array([[np.random.uniform(inf_bound, sup_bound),np.random.uniform(inf_bound, sup_bound)] for i in range(N_test)])
Y_test = np.array([f(O1,O2) for O1,O2 in X_test])
# Neural network prediction
X_pred = model.predict(Y_test) 
X_pred = np.reshape(X_pred,(N_test,2)) 
errors = np.abs(X_test - X_pred)

print('Mean test error :', np.mean(errors[:, 0]))
print('Min test error :', np.min(errors[:, 0]))
print('Max test error :', np.max(errors[:, 0]))