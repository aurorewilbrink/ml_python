import cv2
import numpy as np
import copy
import matplotlib.pyplot as plt
import sys
import os

dir = os.path.join(sys.path[0],"dataset","presenceRecognition")

paths_positif = [os.path.join(dir,"1_{0}.png".format(i)) for i in range(9)]
paths_negatif = [os.path.join(dir,"0_{0}.png".format(i)) for i in range(4)]
paths_test = [os.path.join(dir,"2_{0}.png".format(i)) for i in range(2)]


trainData=[]
responses=[]
for i in range(4):
    path = os.path.join(dir,"0_{0}.png".format(i))
    img = cv2.imread(path,cv2.IMREAD_UNCHANGED)
    print(path)
    trainData.append(np.matrix.flatten(img))
    responses.append(0)
for i in range(9):
    path = os.path.join(dir,"1_{0}.png".format(i))
    img = cv2.imread(path,cv2.IMREAD_UNCHANGED)
    trainData.append(np.matrix.flatten(img))
    responses.append(1)

knn = cv2.ml.KNearest_create()

trainData = np.array(trainData).astype(np.float32)
responses = np.array(responses).astype(np.float32)
knn.train(trainData,cv2.ml.ROW_SAMPLE,responses)
tests= []
for i in range(2):
    path = os.path.join(dir,"2_{0}.png".format(i))
    img = cv2.imread(path,cv2.IMREAD_UNCHANGED)
    tests.append(np.matrix.flatten(img))
tests = np.array(tests).astype(np.float32)
ret, results, neighbours, dist = knn.findNearest(tests, 1)
print(results)