import numpy
import random
import numpy
import matplotlib.pyplot as plt
from sklearn.datasets import load_digits
from sklearn.model_selection import train_test_split
from sklearn.neighbors import KNeighborsClassifier

#data form
digits=load_digits()  # get dataset of digits

images=digits.images  # get image from dataset
n = len(images)  # Get len of image

X=digits.images.reshape((n, -1))  # Reshape image as a vector
Y=digits.target  # Get target of input
Z=digits.target_names  # Get class name of target

X_train,X_test,Y_train,Y_test=train_test_split(X,Y)  # split optimaly datas
knn=KNeighborsClassifier(n_neighbors=5)  # create the neural network
knn.fit(X_train,Y_train)  # train the neural network

score=knn.score(X_test,Y_test)
print('score',score)

#test new
i=random.randint(0,n)
image_new=images[i]  # get new value
X_new=numpy.matrix.flatten(image_new)
prediction_index = knn.predict([X_new])  # calcul the output
prediction=Z[prediction_index]  # claasified the new value
print('prediction',prediction)
plt.imshow(image_new)
plt.show()