import sys
import time
import os
import copy
import numpy as np
from collections import Counter
import tensorflow as tf

spam = "spam\t"
ham = "ham\t"

print(time.time(),"OUVERTURE DU FICHIER")
dir = sys.path[0]
path = os.path.join(dir,"dataset","spamRecognition","spamRecognition.txt")
file = open(path, "r")
text = file.read().replace(spam,"").replace(ham,"")
file.close()
file = open(path, "r")
lines = file.readlines()

print(time.time(),"INITIALISATION")

X_train = []  # filtered inputs
Y_train = []  # Outputs
X_test = []
Y_test = []
Z1 = []  # Dictionnary
Z2 = []  # Dictionnary counter

print(time.time(),"CREATION DICTIONNAIRE")
# text traitment
text = text.replace("\n"," ")
words = text.split(" ")
words = filter(lambda word: word != "", words)
dico = Counter(words)
keys = dico.keys()

print(time.time(),"CREATION LISTE DE MOTS")
for i in dico:
    Z1.append(i)
    Z2.append(dico[i])
Z1 = [x for _, x in sorted(zip(Z2, Z1), reverse=True)]
Z2 = sorted(Z2, reverse=True)

print(time.time(),"FORMATAGE DES DONNEES")
for i in range(len(lines)):
    line = lines[i]
    data = line.replace(spam,"").replace(ham,"").replace("\n"," ")
    words = data.split(" ")
    if i < len(lines)/2:
        L = [X_train,Y_train]
    else:
        L = [X_test,Y_test]
    
    L[0].append([word in words for word in Z1])
    L[1].append([int(line[:len(ham)] == ham)])

try:
    model = tf.keras.models.load_model(os.path.join(sys.path[0],'classification_spam_NN_tensorflow.model'))
except:
    print(time.time(),"CREATION DU RESEAU DE NEURONE")
    model = tf.keras.models.Sequential()
    model.add(tf.keras.layers.Flatten())
    print(time.time(),"AJOUT COUCHE 1")
    model.add(tf.keras.layers.Dense(len(Z1),activation=tf.nn.relu))
    print(time.time(),"AJOUT COUCHE 2")
    model.add(tf.keras.layers.Dense(len(Z1)//10,activation=tf.nn.relu))
    print(time.time(),"AJOUT COUCHE 3")
    model.add(tf.keras.layers.Dense(len(Z1)//100,activation=tf.nn.relu))
    print(time.time(),"AJOUT COUCHE 4")
    model.add(tf.keras.layers.Dense(2,activation=tf.nn.softmax))

    print(time.time(),"COMPILATION DU MODELE")
    model.compile(
        optimizer='adam',
        loss='sparse_categorical_crossentropy',
        metrics=['accuracy']
    )
    print(time.time(),"ENTRAINEMENT")
    history = model.fit(
        X_train,
        Y_train,
        epochs=2
    )
print(time.time(),"EVALUATION DU MODELE")
val_loss,val_acc = model.evaluate(X_test,Y_test)
print(time.time(),"RESULTATS")
model.summary()